1.Avant de partir le fichier (tp3.py - cr�er la BD), il faut que les fichiers dictionnaire.csv et matrice.csv soit 
  d�j� cr�er. (Des fichiers d�j� remplis sont remis)

2.Pour lancer l'entrainement � la ligne de commande il faut specifier le mode -e, la taille -t , l'encodage -enc et les textes -cc
  ex. python tp3.py -e -t 5 -enc utf-8 -cc textes\GerminalUTF8.txt

3.Avoir le fichier cluster.py, stopliste.txt, tp3.py, dictionnaire.csv, matrice.csv et le dossier de textes
  au m�me endroit.

4. Ouvrir la ligne de commande appel� le fichier cluster.py -t 5 (on a qu'une taille de 5 enregistrer)
   -n (le nombre de mots � sortir par centro�de) et -nc (nombre de centro�de al�atoir) ou -m (liste de mots
    s�par�s pas un espace pour partir les centro�de).
    
    ex. python Cluster.py -t 5 -n 10 -nc 5 > testAleatoire.txt



NOTE 

Dans le dossier "LesTests" ---> contient tous nos tests effectu�s
Dans le dossier "rapport" ---> contient l'article scientifique, des graphiques et des tableaux (utilis� dans l'article)
Dans le dossier "textes" ---> contient tous les textes utilis�s
