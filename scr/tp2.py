#-*- coding: utf-8 -*-
import sys
import re
import numpy as np
from time import time 
import cx_Oracle

class Dicto():
	def __init__(self):
		self.listemots=[]
		self.diction=dict()
		self.matrix= []
		self.tailleDic = 0
		self.stoplist =[]
		self.stoplistNumerique=[]
		self.listCorrespondance=[]
		self.tailleDesFenetre=[]
		self.connexion = None
		self.tailleFenetre=0
		self.matrixExistance = dict()	
				
	def connectionBD(self):
		PATH_ORACLE = 'C:\Oracle\Client\product\12.2.0\client_1\bin'
		sys.path.append(PATH_ORACLE)
		dsn_tns=cx_Oracle.makedsn('delta',1521,'decinfo')

		#chaineConnexion='E1142143' +'/' + "Claudesql" + '@' + dsn_tns #Claude
		chaineConnexion='E1426435' +'/' + "KL" + '@' + dsn_tns #Karine

		self.connexion =cx_Oracle.connect(chaineConnexion)

		return 0
	
	def deconnectionBD(self):
		self.connexion.close()
		return 0

	def LireDict (self):
		requete = "SELECT * FROM dictionnaire"
		cur = self.connexion.cursor()
		cur.execute(requete)
		for mot in cur.fetchall() :
			self.diction.update({mot[1]:mot[0]})

			if mot[1] in self.stoplist:
				self.stoplistNumerique.append(self.tailleDic)

			self.tailleDic+=1
		cur.close()
		
	def tailleDispo(self):
		requete = "SELECT DISTINCT fenetre FROM matrice"
		cur = self.connexion.cursor()
		cur.execute(requete)

		tailleX = cur.fetchall()

		for tailleY in tailleX:
			if tailleY[0]!=None:
				self.tailleDesFenetre.append(int(tailleY[0]))

		cur.close()
		

	def remplirDict(self,textePath,enc):
		nouveauMot = []

		ficher=open(textePath,'r',encoding=enc)
		p = re.compile("[ « » , . ( ) \" ? ! ; ' : \r \n ]+") #caractère couper
		texte=ficher.read()
		texte = texte[1:]#problème de caractère spéciaux
		d = 0
		m = p.search(texte, d)

		while m!=None:
			motTrouver = False
			f = m.start()
			mot=texte[d:f].lower()	
			motTrouver = False
			curAjout = self.connexion.cursor()
			if mot in self.diction:
				motTrouver = True
				self.listemots.append(self.diction[mot])
			if motTrouver == False:
				self.diction.update({mot:self.tailleDic})
				self.listemots.append(self.tailleDic)
				nouveauMot.append((self.tailleDic,mot))

				if mot in self.stoplist:
					self.stoplistNumerique.append(self.tailleDic)
				self.tailleDic+=1	
			d=m.end()
			m = p.search(texte, d)


		insertion = "INSERT INTO dictionnaire (position, mot) VALUES (:1,:2)"
		params = nouveauMot

		curAjout.executemany(insertion,params)
		self.connexion.commit()
		curAjout.close()
		ficher.close()
				
	def remplirStoplist(self):
		ficher=open("stopliste.txt",'r',encoding="utf-8")
		texte=ficher.read()
		texte = texte[1:]
		self.stoplist=texte.split()

		ficher.close()
	
	def creerMatrice(self):
		self.matrix = np.zeros((self.tailleDic,self.tailleDic))
		self.lireMatrixBD()

		x=0
		taille = len(self.listemots)
		for indexMot in self.listemots:
			i=1
			for i in range (1,self.fenetreVerif+1):
				if x+i< taille:
					self.matrix[indexMot][self.listemots[x+i]]+=1
					self.matrix[self.listemots[x+i]][indexMot]+=1
			x+=1

		self.ajoutMatrixBD()
		return 0
	
	def lireMatrixBD(self):
		mot1 = None
		mot2 = None
		score = None
		cur = self.connexion.cursor()
		lire = 'SELECT * FROM matrice WHERE fenetre= :1'
		cur.execute(lire, str(self.tailleFenetre))
		rangee=cur.fetchone()
		while rangee!=None:
			mot1=rangee[0]
			mot2=rangee[1]
			score=rangee[2]

			self.matrix[mot1][mot2]=score
			self.matrixExistance.update({(mot1,mot2):score})
			rangee=cur.fetchone()
	
	def ajoutMatrixBD(self):
		cur = self.connexion.cursor()
		ajout = 'INSERT INTO matrice(mot1,mot2,score,fenetre) VALUES(:1,:2,:3,:4)' 
		update = 'UPDATE matrice SET score = :1 WHERE mot1 = :2 AND mot2=:3 AND fenetre=:4'
		params = []
		paramsModif=[]
		valeur = np.argwhere(self.matrix>0)
		for xy in valeur:
		
			score = self.matrix[xy[0]][xy[1]]
			motxy = (xy[0],xy[1])

			if motxy in self.matrixExistance:
				if self.matrixExistance[motxy]!=score:
					paramsModif.append((score,int(xy[0]),int(xy[1]),self.tailleFenetre))
			else:
				params.append((int(xy[0]),int(xy[1]),score,self.tailleFenetre))

		cur.executemany(ajout,params)
		cur.executemany(update,paramsModif)
		self.connexion.commit()
		return 0
			
	def trouverSynonyme (self,choix, mots):
		self.matrix = np.zeros((self.tailleDic,self.tailleDic))
		self.lireMatrixBD()
		indexMots=self.diction[mots]
		motMatrix = self.matrix[indexMots]
		listeScore=[]
		i=0

		for ligne in self.matrix:
			score = 0
			if  i!=indexMots:
				if choix == 0 :
					score=np.dot(motMatrix,ligne)
				if choix == 1 : 
					matxResult=(motMatrix-ligne)
					score = np.sum(np.square(matxResult))
				if choix == 2:
					matxResult=np.abs(motMatrix-ligne)
					score = np.sum(matxResult)
			listeScore.append((score,i))
			i+=1	
		if choix == 0 :	
			listeScore=sorted(listeScore,reverse=True)
		else : 
			listeScore=sorted(listeScore,reverse=False)
	
		return listeScore


	def affichageSyno(self,listeScore,nbSyno,motDemander):
		z=0
		nbS = 0
		while nbS<nbSyno:
			for mot,index in self.diction.items():
				if z< len(listeScore):
					if index == listeScore[z][1]:	
						if mot  not in self.stoplist and mot != motDemander:	
							print(mot + " : " + str(listeScore[z][0]))
							nbS+=1
						z+=1	
						break
				else:
					return 0
		return 0 



def main():
	dernierArg=None;
	action = None #"e","s"
	optionFenetre = "t"
	optionEnco = "enc"
	optionText = "cc"
	chemins=[]
	continuer=True
	dicto = Dicto()
	nomMethode="\n####produit Scalaire####","\n####Écart-Type#####","\n#####city-block####"
	dicto.connectionBD();

	try:
		for argument in sys.argv[1:]:
			if argument[0]=='-':
				if argument[1:]=="e":
					action = "e"
				elif argument[1:]=="s":
					action = "s"
				elif argument[1:]=="t":
					dernierArg = "t"
				elif argument[1:]=="enc":
					dernierArg = "enc"
				elif argument[1:]=="cc":
					dernierArg = "cc"
			
			elif dernierArg!=None:
				if dernierArg == optionFenetre:
					fenetre = argument
				elif dernierArg == optionEnco:
					encodage = argument
				elif dernierArg == optionText:
					chemins.append(argument)

		dicto.tailleFenetre=int(fenetre)		
	
		dicto.fenetreVerif=int(np.floor(int(fenetre)/2)) #pour avoir le nombre de mots avant et apres
		
		dicto.remplirStoplist()
		dicto.LireDict()
		dicto.tailleDispo()

	except Exception as e:
		print ("\nParamètre invalide!")
		dicto.deconnectionBD()
		return 0



	if action == "e" :
		if len(chemins)!=0:
			try:
				print("\nEn cours de traitement...")

				for textChemin in chemins:
					dicto.remplirDict(textChemin,encodage)

				
				dicto.creerMatrice()

			except FileNotFoundError:
				print ("\nTexte non trouvé!")
			except UnboundLocalError:
				print ("\nArgument manquant")

		else:
			print("\nArgument manquant pour les textes")

	elif action == "s" 	and (dicto.tailleFenetre in dicto.tailleDesFenetre):

		print("\nEntrez un mot, le nombre de synonyme que vous voulez et le méthode de calcul parmi les suivantes")
		print("Méthode de calcul: produit scalaire: 0 , least sqares: 1 , cityblock: 2")
	
		while continuer:
			print("\nTaper -1 pour quitter")
			
			information=input("\nEntrer votre mot: ")
			information = information.split()

			motRecherche=information[0]
			motRecherche.lower()
		
			if motRecherche == '-1':
				continuer=False
				pass
			elif motRecherche in dicto.diction:
				try:
						nbSyno = int(information[1])

						meth = int(information[2])

						print(nomMethode[meth])
						listeScore= dicto.trouverSynonyme(meth, motRecherche)
						dicto.affichageSyno(listeScore,nbSyno,motRecherche)
						

				except IndexError:
					print ("\nParamètre invalide en dehors des index! Veuillez réessayer!")
				except ValueError:
					print ("\nParamètre invalide! Veuillez réessayer!")
			else:
				print("\nMot inconnu")
				


	else:
		if action != "e" and action != "s":
			print("\nAction inconnu choisir -e ou -s")
		else:
			print("\nErreur dans les arguments")
	
	dicto.deconnectionBD()
	return 0



if __name__ == '__main__':
	sys.exit(main())
