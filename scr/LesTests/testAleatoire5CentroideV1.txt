python Cluster.py -t 5 -n 10 -nc 5 > testAleatoire5CentroideV1.txt
5
Il y a 1393 points (mot) regroupes autour du centroide no 0
Il y a 640 points (mot) regroupes autour du centroide no 1
Il y a 23275 points (mot) regroupes autour du centroide no 2
Il y a 952 points (mot) regroupes autour du centroide no 3
Il y a 981 points (mot) regroupes autour du centroide no 4
============================================

Iteration 0 terminee. 3081 changements de clusters

Il y a 287 points (mot) regroupes autour du centroide no 0
Il y a 2619 points (mot) regroupes autour du centroide no 1
Il y a 23683 points (mot) regroupes autour du centroide no 2
Il y a 348 points (mot) regroupes autour du centroide no 3
Il y a 304 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 0 est de: 7.5936338901519775 secondes

============================================

Iteration 1 terminee. 1013 changements de clusters

Il y a 154 points (mot) regroupes autour du centroide no 0
Il y a 2498 points (mot) regroupes autour du centroide no 1
Il y a 23951 points (mot) regroupes autour du centroide no 2
Il y a 543 points (mot) regroupes autour du centroide no 3
Il y a 95 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 1 est de: 7.546751976013184 secondes

============================================

Iteration 2 terminee. 550 changements de clusters

Il y a 195 points (mot) regroupes autour du centroide no 0
Il y a 2203 points (mot) regroupes autour du centroide no 1
Il y a 24320 points (mot) regroupes autour du centroide no 2
Il y a 472 points (mot) regroupes autour du centroide no 3
Il y a 51 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 2 est de: 7.5311360359191895 secondes

============================================

Iteration 3 terminee. 339 changements de clusters

Il y a 163 points (mot) regroupes autour du centroide no 0
Il y a 1998 points (mot) regroupes autour du centroide no 1
Il y a 24562 points (mot) regroupes autour du centroide no 2
Il y a 480 points (mot) regroupes autour du centroide no 3
Il y a 38 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 3 est de: 7.5623860359191895 secondes

============================================

Iteration 4 terminee. 314 changements de clusters

Il y a 113 points (mot) regroupes autour du centroide no 0
Il y a 1904 points (mot) regroupes autour du centroide no 1
Il y a 24733 points (mot) regroupes autour du centroide no 2
Il y a 461 points (mot) regroupes autour du centroide no 3
Il y a 30 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 4 est de: 7.796766996383667 secondes

============================================

Iteration 5 terminee. 312 changements de clusters

Il y a 76 points (mot) regroupes autour du centroide no 0
Il y a 1801 points (mot) regroupes autour du centroide no 1
Il y a 24916 points (mot) regroupes autour du centroide no 2
Il y a 424 points (mot) regroupes autour du centroide no 3
Il y a 24 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 5 est de: 7.85924506187439 secondes

============================================

Iteration 6 terminee. 325 changements de clusters

Il y a 55 points (mot) regroupes autour du centroide no 0
Il y a 1685 points (mot) regroupes autour du centroide no 1
Il y a 25119 points (mot) regroupes autour du centroide no 2
Il y a 365 points (mot) regroupes autour du centroide no 3
Il y a 17 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 6 est de: 7.843632936477661 secondes

============================================

Iteration 7 terminee. 305 changements de clusters

Il y a 44 points (mot) regroupes autour du centroide no 0
Il y a 1547 points (mot) regroupes autour du centroide no 1
Il y a 25332 points (mot) regroupes autour du centroide no 2
Il y a 304 points (mot) regroupes autour du centroide no 3
Il y a 14 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 7 est de: 7.6873860359191895 secondes

============================================

Iteration 8 terminee. 314 changements de clusters

Il y a 42 points (mot) regroupes autour du centroide no 0
Il y a 1381 points (mot) regroupes autour du centroide no 1
Il y a 25567 points (mot) regroupes autour du centroide no 2
Il y a 241 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 8 est de: 7.9061279296875 secondes

============================================

Iteration 9 terminee. 254 changements de clusters

Il y a 38 points (mot) regroupes autour du centroide no 0
Il y a 1221 points (mot) regroupes autour du centroide no 1
Il y a 25772 points (mot) regroupes autour du centroide no 2
Il y a 200 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 9 est de: 8.129159927368164 secondes

============================================

Iteration 10 terminee. 224 changements de clusters

Il y a 36 points (mot) regroupes autour du centroide no 0
Il y a 1061 points (mot) regroupes autour du centroide no 1
Il y a 25963 points (mot) regroupes autour du centroide no 2
Il y a 171 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 10 est de: 8.360409021377563 secondes

============================================

Iteration 11 terminee. 188 changements de clusters

Il y a 35 points (mot) regroupes autour du centroide no 0
Il y a 926 points (mot) regroupes autour du centroide no 1
Il y a 26124 points (mot) regroupes autour du centroide no 2
Il y a 146 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 11 est de: 8.221938133239746 secondes

============================================

Iteration 12 terminee. 161 changements de clusters

Il y a 35 points (mot) regroupes autour du centroide no 0
Il y a 813 points (mot) regroupes autour du centroide no 1
Il y a 26261 points (mot) regroupes autour du centroide no 2
Il y a 122 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 12 est de: 8.095834970474243 secondes

============================================

Iteration 13 terminee. 121 changements de clusters

Il y a 35 points (mot) regroupes autour du centroide no 0
Il y a 736 points (mot) regroupes autour du centroide no 1
Il y a 26360 points (mot) regroupes autour du centroide no 2
Il y a 100 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 13 est de: 8.028745889663696 secondes

============================================

Iteration 14 terminee. 108 changements de clusters

Il y a 34 points (mot) regroupes autour du centroide no 0
Il y a 643 points (mot) regroupes autour du centroide no 1
Il y a 26460 points (mot) regroupes autour du centroide no 2
Il y a 94 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 14 est de: 7.949249982833862 secondes

============================================

Iteration 15 terminee. 81 changements de clusters

Il y a 33 points (mot) regroupes autour du centroide no 0
Il y a 587 points (mot) regroupes autour du centroide no 1
Il y a 26528 points (mot) regroupes autour du centroide no 2
Il y a 83 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 15 est de: 7.857140064239502 secondes

============================================

Iteration 16 terminee. 77 changements de clusters

Il y a 32 points (mot) regroupes autour du centroide no 0
Il y a 541 points (mot) regroupes autour du centroide no 1
Il y a 26589 points (mot) regroupes autour du centroide no 2
Il y a 69 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 16 est de: 7.961532115936279 secondes

============================================

Iteration 17 terminee. 77 changements de clusters

Il y a 31 points (mot) regroupes autour du centroide no 0
Il y a 487 points (mot) regroupes autour du centroide no 1
Il y a 26654 points (mot) regroupes autour du centroide no 2
Il y a 59 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 17 est de: 8.24181580543518 secondes

============================================

Iteration 18 terminee. 46 changements de clusters

Il y a 31 points (mot) regroupes autour du centroide no 0
Il y a 449 points (mot) regroupes autour du centroide no 1
Il y a 26696 points (mot) regroupes autour du centroide no 2
Il y a 55 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 18 est de: 7.990927219390869 secondes

============================================

Iteration 19 terminee. 29 changements de clusters

Il y a 31 points (mot) regroupes autour du centroide no 0
Il y a 426 points (mot) regroupes autour du centroide no 1
Il y a 26722 points (mot) regroupes autour du centroide no 2
Il y a 52 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 19 est de: 7.767085790634155 secondes

============================================

Iteration 20 terminee. 28 changements de clusters

Il y a 31 points (mot) regroupes autour du centroide no 0
Il y a 404 points (mot) regroupes autour du centroide no 1
Il y a 26747 points (mot) regroupes autour du centroide no 2
Il y a 49 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 20 est de: 7.856188058853149 secondes

============================================

Iteration 21 terminee. 15 changements de clusters

Il y a 30 points (mot) regroupes autour du centroide no 0
Il y a 396 points (mot) regroupes autour du centroide no 1
Il y a 26758 points (mot) regroupes autour du centroide no 2
Il y a 47 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 21 est de: 7.781131982803345 secondes

============================================

Iteration 22 terminee. 14 changements de clusters

Il y a 28 points (mot) regroupes autour du centroide no 0
Il y a 390 points (mot) regroupes autour du centroide no 1
Il y a 26767 points (mot) regroupes autour du centroide no 2
Il y a 46 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 22 est de: 7.730530023574829 secondes

============================================

Iteration 23 terminee. 20 changements de clusters

Il y a 27 points (mot) regroupes autour du centroide no 0
Il y a 377 points (mot) regroupes autour du centroide no 1
Il y a 26783 points (mot) regroupes autour du centroide no 2
Il y a 44 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 23 est de: 7.819584131240845 secondes

============================================

Iteration 24 terminee. 20 changements de clusters

Il y a 25 points (mot) regroupes autour du centroide no 0
Il y a 367 points (mot) regroupes autour du centroide no 1
Il y a 26797 points (mot) regroupes autour du centroide no 2
Il y a 42 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 24 est de: 7.801430940628052 secondes

============================================

Iteration 25 terminee. 24 changements de clusters

Il y a 24 points (mot) regroupes autour du centroide no 0
Il y a 360 points (mot) regroupes autour du centroide no 1
Il y a 26812 points (mot) regroupes autour du centroide no 2
Il y a 35 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 25 est de: 7.871412038803101 secondes

============================================

Iteration 26 terminee. 30 changements de clusters

Il y a 23 points (mot) regroupes autour du centroide no 0
Il y a 339 points (mot) regroupes autour du centroide no 1
Il y a 26837 points (mot) regroupes autour du centroide no 2
Il y a 32 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 26 est de: 7.746910810470581 secondes

============================================

Iteration 27 terminee. 27 changements de clusters

Il y a 21 points (mot) regroupes autour du centroide no 0
Il y a 320 points (mot) regroupes autour du centroide no 1
Il y a 26859 points (mot) regroupes autour du centroide no 2
Il y a 31 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 27 est de: 7.718619108200073 secondes

============================================

Iteration 28 terminee. 15 changements de clusters

Il y a 21 points (mot) regroupes autour du centroide no 0
Il y a 313 points (mot) regroupes autour du centroide no 1
Il y a 26870 points (mot) regroupes autour du centroide no 2
Il y a 27 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 28 est de: 7.718632936477661 secondes

============================================

Iteration 29 terminee. 17 changements de clusters

Il y a 21 points (mot) regroupes autour du centroide no 0
Il y a 298 points (mot) regroupes autour du centroide no 1
Il y a 26886 points (mot) regroupes autour du centroide no 2
Il y a 26 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 29 est de: 7.840862989425659 secondes

============================================

Iteration 30 terminee. 9 changements de clusters

Il y a 21 points (mot) regroupes autour du centroide no 0
Il y a 291 points (mot) regroupes autour du centroide no 1
Il y a 26894 points (mot) regroupes autour du centroide no 2
Il y a 25 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 30 est de: 7.783783197402954 secondes

============================================

Iteration 31 terminee. 5 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 287 points (mot) regroupes autour du centroide no 1
Il y a 26898 points (mot) regroupes autour du centroide no 2
Il y a 26 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 31 est de: 7.796761989593506 secondes

============================================

Iteration 32 terminee. 5 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 282 points (mot) regroupes autour du centroide no 1
Il y a 26903 points (mot) regroupes autour du centroide no 2
Il y a 26 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 32 est de: 7.781117916107178 secondes

============================================

Iteration 33 terminee. 4 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 278 points (mot) regroupes autour du centroide no 1
Il y a 26907 points (mot) regroupes autour du centroide no 2
Il y a 26 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 33 est de: 7.777145862579346 secondes

============================================

Iteration 34 terminee. 4 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 276 points (mot) regroupes autour du centroide no 1
Il y a 26910 points (mot) regroupes autour du centroide no 2
Il y a 25 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 34 est de: 7.784754037857056 secondes

============================================

Iteration 35 terminee. 4 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 272 points (mot) regroupes autour du centroide no 1
Il y a 26914 points (mot) regroupes autour du centroide no 2
Il y a 25 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 35 est de: 7.7498719692230225 secondes

============================================

Iteration 36 terminee. 4 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 270 points (mot) regroupes autour du centroide no 1
Il y a 26917 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 36 est de: 7.828007221221924 secondes

============================================

Iteration 37 terminee. 2 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 268 points (mot) regroupes autour du centroide no 1
Il y a 26919 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 37 est de: 7.7463719844818115 secondes

============================================

Iteration 38 terminee. 1 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 267 points (mot) regroupes autour du centroide no 1
Il y a 26920 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 38 est de: 7.752466917037964 secondes

============================================

Iteration 39 terminee. 1 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 266 points (mot) regroupes autour du centroide no 1
Il y a 26921 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 39 est de: 7.749881982803345 secondes

============================================

Iteration 40 terminee. 2 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 264 points (mot) regroupes autour du centroide no 1
Il y a 26923 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 40 est de: 7.7498838901519775 secondes

============================================

Iteration 41 terminee. 2 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 262 points (mot) regroupes autour du centroide no 1
Il y a 26925 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 41 est de: 7.809030055999756 secondes

============================================

Iteration 42 terminee. 5 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 257 points (mot) regroupes autour du centroide no 1
Il y a 26930 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 42 est de: 7.735903024673462 secondes

============================================

Iteration 43 terminee. 5 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 252 points (mot) regroupes autour du centroide no 1
Il y a 26935 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 43 est de: 7.812382936477661 secondes

============================================

Iteration 44 terminee. 4 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 248 points (mot) regroupes autour du centroide no 1
Il y a 26939 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 44 est de: 7.746920108795166 secondes

============================================

Iteration 45 terminee. 3 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 245 points (mot) regroupes autour du centroide no 1
Il y a 26942 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 45 est de: 7.876342058181763 secondes

============================================

Iteration 46 terminee. 1 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 244 points (mot) regroupes autour du centroide no 1
Il y a 26943 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 46 est de: 7.7620689868927 secondes

============================================

Iteration 47 terminee. 0 changements de clusters

Il y a 20 points (mot) regroupes autour du centroide no 0
Il y a 244 points (mot) regroupes autour du centroide no 1
Il y a 26943 points (mot) regroupes autour du centroide no 2
Il y a 24 points (mot) regroupes autour du centroide no 3
Il y a 10 points (mot) regroupes autour du centroide no 4

Le temps de l'iteration 47 est de: 7.87890100479126 secondes

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

Groupe : 0

avait 219--> 1787332.24584

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

Groupe : 1

mal 781--> 4718.91350861

joie 2593--> 5062.64124214

parler 1121--> 5700.33277002

col�re 2496--> 5985.47976495

avant 2171--> 6096.46311035

gros 502--> 6181.07860637

faim 775--> 6376.33566647

force 1054--> 6519.34652816

tr�s 556--> 6568.46906392

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

Groupe : 2

series 12--> 0

laws 14--> 0

to 23--> 0

for 25--> 0

any 33--> 0

first 37--> 0

viewing 41--> 0

edit 49--> 0

legal 54--> 0

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

Groupe : 3

bien 691--> 292873.27532

plus 680--> 297270.892562

tout 204--> 304639.220341

comme 233--> 321528.723857

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

Groupe : 4

Clustering en 48 iterations: 386.00016713142395 secondes.
Groupes: 0.19729995727539062 secondes
