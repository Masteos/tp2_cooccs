#-*- coding: utf-8 -*-
import sys
import re
import numpy as np
from time import time 
import csv
from tp3 import Dicto


class Cluster():
	def __init__(self, dicto):
		self.dicto = dicto
		self.listeMots =[]#mot , centroides
		self.listeCentroides=[] #idcentroide, coordonnee
		self.nbChangement = 0
		self.cluster0Ancient=None
		self.cluster0nouveau=None
		self.tempsClustering =0
		self.nbIteration=0

	def creerCentroide (self, nombre):
		centroide = np.random.randint(0, high = nombre, size = self.dicto.tailleDic)
		cordonne =  np.zeros(self.dicto.tailleDic)
		
		for c in range (nombre):
			self.listeCentroides.append((c, cordonne))
		
		for x in range (self.dicto.tailleDic) :
			self.listeMots.append((x,centroide[x],0))
		
		self.CalculeBarycente()	
		


	def recupererCentroide(self,motsCentroides):
		idCentroide = 0
		for mot in motsCentroides:
			indexMot = self.dicto.diction[mot]
			coordonne = self.dicto.matrix[indexMot]
			self.listeCentroides.append((idCentroide,coordonne))
			idCentroide+=1

		self.cluster0Ancient = self.listeCentroides[0]

	def LeastSquare (self):
		scoreCentroides= []
		iterateur = 0
		premierFois=True
		meilleurCentroide = None
		self.nbChangement=0

		if len(self.listeMots)>0:
			premierFois=False


		for ligne in self.dicto.matrix:
			scoreCentroides=[]
			for c in self.listeCentroides:
				diffLigne = (c[1] - ligne)
				score = np.sum(np.square(diffLigne))

				scoreCentroides.append(score)

			#print("score des centroide : "+str(scoreCentroides))
			meilleurCentroide = np.argmin(scoreCentroides)
			if premierFois:
				self.listeMots.append((iterateur,meilleurCentroide,scoreCentroides[meilleurCentroide]))
				self.nbChangement+=1
			else:
				if self.listeMots[iterateur][1] != meilleurCentroide:
					self.nbChangement+=1
					self.listeMots[iterateur] = ((iterateur, meilleurCentroide,scoreCentroides[meilleurCentroide]))
			iterateur = iterateur + 1	


	def CalculeBarycente (self):
		listcentroiTemp=[]
		
		for c in self.listeCentroides:
			moyenneTemporaire = np.zeros((1,self.dicto.tailleDic))
			nbMots = 0
			for mots in self.listeMots:
				if c[0] == mots[1]:
					moyenneTemporaire = moyenneTemporaire + self.dicto.matrix[mots[0]]
					nbMots = nbMots + 1
			if nbMots>0:
				moyenneTemporaire = moyenneTemporaire/nbMots
				listcentroiTemp.append((c[0],moyenneTemporaire))
			else:
				listcentroiTemp.append(c)

		self.listeCentroides=listcentroiTemp

		#self.cluster0nouveau = self.listeCentroides[0]

		#test = np.subtract(self.cluster0Ancient[1],self.cluster0nouveau[1])
		#test = np.sum(test)
		#print(test)
		#if test==0:
		#	print("aucunChangement")


	def compterMots(self):
		for c in self.listeCentroides:
			points = 0
			for m in self.listeMots:
				#print(m[1][0])
				if c[0] == m[1]:
					points = points+1

			print("Il y a " + str(points) + " points (mot) regroupes autour du centroide no " + str(c[0]))
 					
				
	def MotPlusProche(self,nombre):	
		self.nombre = nombre
		for c in self.listeCentroides :
			listeScores = []
			print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n")
			print("Groupe : " + str(c[0])+"\n")
			for mots in self.listeMots:
				if (c[0] == mots[1]):
					listeScores.append((mots[2],mots[0]))#score,index
			listeScores = sorted(listeScores, reverse= True)
			#self.affichageSyno(listeScores,nombre)
			for nb in range (self.nombre) :
				if len(listeScores)!=0:
					scorePrint = listeScores.pop()
					for mot,index in self.dicto.diction.items():
						if index == scorePrint[1]:
							if mot  not in self.dicto.stoplist:
								print(mot + " " + str(scorePrint[1])+ "--> "+ str(scorePrint[0]) + "\n")

	def boucle(self):
		self.nbIteration = 0
		tempBoucle = 0
		self.LeastSquare()
		self.CalculeBarycente()
		self.compterMots()
		while self.nbChangement>0:
			tempBoucle = time()
			self.LeastSquare()
			print("============================================\n")
			print("Iteration " + str(self.nbIteration) + " terminee. " + str(self.nbChangement) + " changements de clusters\n")
			self.compterMots()
			self.CalculeBarycente()

			tempBoucle = time() - tempBoucle
			print("\nLe temps de l'iteration " + str(self.nbIteration) + " est de: " + str(tempBoucle) + " secondes\n")
			self.nbIteration = self.nbIteration + 1

		

def main():
	dernierArg=None
	optionFenetre = "t"
	optionNombreCentroide = "nc"
	optionMots = "m"
	optionNombre = "n" 
	fenetre = 0
	nb = 0
	nombreCentroide = None
	mots = []
	dicto = Dicto()
	cluster = None
	tempsGroupe = 0
	tempsClustering=0
	try:
		for argument in sys.argv[1:]:
			if argument[0]=='-':
				if argument[1:]=="t":
					dernierArg = "t"
				elif argument[1:]=="n":
					dernierArg = "n"
				elif argument[1:]=="nc":
					dernierArg = "nc"
				elif argument[1:]=="m":
					dernierArg = "m"
				
			
			elif dernierArg!=None:
				if dernierArg == optionFenetre:
					fenetre = argument
				elif dernierArg == optionNombre:
					nb = int(argument)
				elif dernierArg == optionNombreCentroide:
					nombreCentroide = argument
				elif dernierArg == optionMots:
					mots.append(argument)
				
					
		dicto.tailleFenetre=int(fenetre)        
	
		dicto.fenetreVerif=int(np.floor(int(fenetre)/2)) #pour avoir le nombre de mots avant et apres
		
		dicto.remplirStoplist()
		
		dicto.LireDict()
		
		dicto.matrix = np.zeros((dicto.tailleDic, dicto.tailleDic));
	
		dicto.lireMatrixBD()
		
		#dicto.tailleDispo()
		cluster = Cluster(dicto)

	except Exception:
		print ("\nParamètre invalide!")
		return 0

	
	if nombreCentroide != None:
		if nb >0:
			
			print (nombreCentroide)
			tempsClustering = time()
			cluster.creerCentroide(int(nombreCentroide))
			cluster.boucle()
			tempsClustering = time() - tempsClustering
			tempsGroupe=time()
			cluster.MotPlusProche(nb)
			tempsGroupe=time()-tempsGroupe

		else :
			print ("\nProbleme encontre")
			


	elif len(mots)!=0:
		if nb >0:
			#try:
			print(mots)
			tempsClustering = time()
			cluster.recupererCentroide(mots)
			cluster.boucle()
			tempsClustering = time() - tempsClustering
			tempsGroupe=time()
			cluster.MotPlusProche(nb)
			tempsGroupe=time()-tempsGroupe
			#except Exception:
				#print ("\nProblème rencontré")

	else :
		print("\nNe peux pas trouver de centroïdes!")
	#dicto.dec
	
	print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
	print("Clustering en "+ str(cluster.nbIteration) +" iterations: " + str(tempsClustering) + " secondes.")
	print("Groupes: " + str(tempsGroupe) + " secondes")

	return 0

if __name__ == '__main__':
	sys.exit(main())
