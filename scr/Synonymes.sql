DROP TABLE matrice;
DROP TABLE dictionnaire;

CREATE TABLE dictionnaire (
    position NUMBER(6),
    mot VARCHAR2(40) NOT NULL,
    CONSTRAINT pk_dictio PRIMARY KEY(position),
    CONSTRAINT uc_position UNIQUE (mot)
);

CREATE TABLE matrice (
    mot1 NUMBER(6),
    mot2 NUMBER(6),
    score NUMBER(6) NOT NULL,
    fenetre NUMBER(2),
    CONSTRAINT pk_matrice PRIMARY KEY(Mot1, Mot2, fenetre),
    CONSTRAINT fk_mot1 FOREIGN KEY (Mot1) REFERENCES dictionnaire(position),
    CONSTRAINT fk_mot2 FOREIGN KEY (Mot2) REFERENCES dictionnaire (position)
);


